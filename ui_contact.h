/********************************************************************************
** Form generated from reading UI file 'contact.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTACT_H
#define UI_CONTACT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_contact
{
public:
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QListWidget *listWidget;
    QWidget *widget;
    QPushButton *closeBtn;

    void setupUi(QWidget *contact)
    {
        if (contact->objectName().isEmpty())
            contact->setObjectName(QString::fromUtf8("contact"));
        contact->resize(400, 350);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(contact->sizePolicy().hasHeightForWidth());
        contact->setSizePolicy(sizePolicy);
        contact->setMinimumSize(QSize(400, 350));
        contact->setMaximumSize(QSize(400, 350));
        scrollArea = new QScrollArea(contact);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(60, 40, 280, 310));
        sizePolicy.setHeightForWidth(scrollArea->sizePolicy().hasHeightForWidth());
        scrollArea->setSizePolicy(sizePolicy);
        scrollArea->setMinimumSize(QSize(280, 310));
        scrollArea->setMaximumSize(QSize(280, 310));
        scrollArea->setFrameShape(QFrame::NoFrame);
        scrollArea->setLineWidth(1);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 280, 310));
        listWidget = new QListWidget(scrollAreaWidgetContents);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(0, 0, 280, 310));
        listWidget->setFrameShape(QFrame::NoFrame);
        listWidget->setFrameShadow(QFrame::Sunken);
        scrollArea->setWidget(scrollAreaWidgetContents);
        widget = new QWidget(contact);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(0, 0, 400, 27));
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMinimumSize(QSize(400, 27));
        widget->setMaximumSize(QSize(400, 27));
        closeBtn = new QPushButton(widget);
        closeBtn->setObjectName(QString::fromUtf8("closeBtn"));
        closeBtn->setGeometry(QRect(373, 0, 27, 27));
        sizePolicy.setHeightForWidth(closeBtn->sizePolicy().hasHeightForWidth());
        closeBtn->setSizePolicy(sizePolicy);
        closeBtn->setMinimumSize(QSize(27, 27));
        closeBtn->setMaximumSize(QSize(27, 27));

        retranslateUi(contact);

        QMetaObject::connectSlotsByName(contact);
    } // setupUi

    void retranslateUi(QWidget *contact)
    {
        contact->setWindowTitle(QCoreApplication::translate("contact", "Form", nullptr));
        closeBtn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class contact: public Ui_contact {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTACT_H
