﻿#include "contactitem.h"
#include "ui_contactitem.h"

contactItem::contactItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::contactItem)
{
    ui->setupUi(this);
}

void contactItem::setUsername(QString str)
{
    ui->nameLabel->setText(str);
}

void contactItem::setHeadPicture(QPixmap pixmap)
{
    ui->headLabel->setPixmap(pixmap);
}

QSize contactItem::getHeadSize()
{
    return ui->headLabel->size();
}

contactItem::~contactItem()
{
    delete ui;
}
