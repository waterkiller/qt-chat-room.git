﻿#ifndef HOME_H
#define HOME_H

#include <QWidget>
#include<QMouseEvent>
#include"contact.h"
namespace Ui {
class home;
}

class home : public QWidget
{
    Q_OBJECT

public:
    explicit home(QWidget *parent = nullptr);
    QPixmap getRoundImg(const QPixmap & src,QPixmap & mask, QSize maskSize);//获取圆头像
    ~home();
private:
    void loadStyleSheet();
    void initControl();
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
private slots:
    void on_closeBtn_clicked();

    void on_chatBtn_clicked();

private:
    Ui::home *ui;
    QPoint m_point;
    contact*m_contact;
};

#endif // HOME_H
