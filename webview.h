﻿#ifndef WEBVIEW_H
#define WEBVIEW_H

#include<QObject>
#include <QWebEngineView>
#include <QDomNode>
#include <QWebEnginePage>

class msgHtmlObj :public QObject {
    Q_OBJECT
    Q_PROPERTY(QString msgLHtmlTmpl MEMBER m_msgLHtml NOTIFY signalMsgHtml)
    Q_PROPERTY(QString msgRHtmlTmpl MEMBER m_msgRHtml NOTIFY signalMsgHtml)
public:
    msgHtmlObj(QObject*parent);
signals:
    void signalMsgHtml(const QString&html);
private:
    QString m_msgLHtml;//左边发来的信息
    QString m_msgRHtml;//右边自己发出的信息
private:
    void initHtml();//初始化聊天网页
    QString getMsgHtml(const QString&code);
};



class webView : public QWebEngineView
{
    Q_OBJECT

public:
    webView(QWidget *parent);
    ~webView();
    void appendMsg(const QString&html,QString obj="0");
private:
    msgHtmlObj*m_msgHtmlObj;
    QWebChannel*m_channel;
};


#endif // WEBVIEW_H
