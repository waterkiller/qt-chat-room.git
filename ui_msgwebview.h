﻿/********************************************************************************
** Form generated from reading UI file 'msgwebview.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MSGWEBVIEW_H
#define UI_MSGWEBVIEW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>
#include <webview.h>

QT_BEGIN_NAMESPACE

class Ui_msgWebView
{
public:
    QWidget *widget;
    QPushButton *closeBtn;
    QLabel *label;
    webView *textBrowser;
    QTextEdit *textEdit;
    QPushButton *sendBtn;

    void setupUi(QWidget *msgWebView)
    {
        if (msgWebView->objectName().isEmpty())
            msgWebView->setObjectName(QString::fromUtf8("msgWebView"));
        msgWebView->resize(400, 400);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(msgWebView->sizePolicy().hasHeightForWidth());
        msgWebView->setSizePolicy(sizePolicy);
        msgWebView->setMinimumSize(QSize(400, 400));
        msgWebView->setMaximumSize(QSize(400, 400));
        widget = new QWidget(msgWebView);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(0, 0, 400, 27));
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMinimumSize(QSize(400, 27));
        widget->setMaximumSize(QSize(400, 27));
        closeBtn = new QPushButton(widget);
        closeBtn->setObjectName(QString::fromUtf8("closeBtn"));
        closeBtn->setGeometry(QRect(373, 0, 27, 27));
        sizePolicy.setHeightForWidth(closeBtn->sizePolicy().hasHeightForWidth());
        closeBtn->setSizePolicy(sizePolicy);
        closeBtn->setMinimumSize(QSize(27, 27));
        closeBtn->setMaximumSize(QSize(27, 27));
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 0, 72, 27));
        textBrowser = new webView(msgWebView);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(0, 27, 400, 250));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(textBrowser->sizePolicy().hasHeightForWidth());
        textBrowser->setSizePolicy(sizePolicy1);
        textBrowser->setMinimumSize(QSize(400, 0));
        textBrowser->setMaximumSize(QSize(400, 16777215));
        //textBrowser->setFrameShape(QFrame::NoFrame);
        textEdit = new QTextEdit(msgWebView);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(0, 277, 400, 80));
        sizePolicy1.setHeightForWidth(textEdit->sizePolicy().hasHeightForWidth());
        textEdit->setSizePolicy(sizePolicy1);
        textEdit->setMinimumSize(QSize(400, 0));
        textEdit->setMaximumSize(QSize(400, 16777215));
        textEdit->setFrameShape(QFrame::NoFrame);
        sendBtn = new QPushButton(msgWebView);
        sendBtn->setObjectName(QString::fromUtf8("sendBtn"));
        sendBtn->setGeometry(QRect(290, 365, 90, 30));
        sizePolicy.setHeightForWidth(sendBtn->sizePolicy().hasHeightForWidth());
        sendBtn->setSizePolicy(sizePolicy);
        sendBtn->setMinimumSize(QSize(90, 30));
        sendBtn->setMaximumSize(QSize(90, 30));
        sendBtn->setFlat(true);

        retranslateUi(msgWebView);

        QMetaObject::connectSlotsByName(msgWebView);
    } // setupUi

    void retranslateUi(QWidget *msgWebView)
    {
        msgWebView->setWindowTitle(QCoreApplication::translate("msgWebView", "Form", nullptr));
        closeBtn->setText(QString());
        label->setText(QCoreApplication::translate("msgWebView", "\350\201\212\345\244\251\345\256\244", nullptr));
        sendBtn->setText(QCoreApplication::translate("msgWebView", "\345\217\221\351\200\201", nullptr));
    } // retranslateUi

};

namespace Ui {
    class msgWebView: public Ui_msgWebView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MSGWEBVIEW_H
