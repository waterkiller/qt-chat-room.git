/********************************************************************************
** Form generated from reading UI file 'contactitem.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTACTITEM_H
#define UI_CONTACTITEM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_contactItem
{
public:
    QLabel *headLabel;
    QLabel *nameLabel;

    void setupUi(QWidget *contactItem)
    {
        if (contactItem->objectName().isEmpty())
            contactItem->setObjectName(QString::fromUtf8("contactItem"));
        contactItem->resize(280, 60);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(contactItem->sizePolicy().hasHeightForWidth());
        contactItem->setSizePolicy(sizePolicy);
        contactItem->setMinimumSize(QSize(280, 60));
        contactItem->setMaximumSize(QSize(280, 60));
        headLabel = new QLabel(contactItem);
        headLabel->setObjectName(QString::fromUtf8("headLabel"));
        headLabel->setGeometry(QRect(30, 5, 50, 50));
        sizePolicy.setHeightForWidth(headLabel->sizePolicy().hasHeightForWidth());
        headLabel->setSizePolicy(sizePolicy);
        headLabel->setMinimumSize(QSize(50, 50));
        headLabel->setMaximumSize(QSize(50, 50));
        nameLabel = new QLabel(contactItem);
        nameLabel->setObjectName(QString::fromUtf8("nameLabel"));
        nameLabel->setGeometry(QRect(100, 20, 151, 35));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(nameLabel->sizePolicy().hasHeightForWidth());
        nameLabel->setSizePolicy(sizePolicy1);
        nameLabel->setMinimumSize(QSize(0, 35));
        nameLabel->setMaximumSize(QSize(16777215, 35));
        nameLabel->setStyleSheet(QString::fromUtf8("font: 10pt \"Times New Roman\";"));

        retranslateUi(contactItem);

        QMetaObject::connectSlotsByName(contactItem);
    } // setupUi

    void retranslateUi(QWidget *contactItem)
    {
        contactItem->setWindowTitle(QCoreApplication::translate("contactItem", "Form", nullptr));
        headLabel->setText(QString());
        nameLabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class contactItem: public Ui_contactItem {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTACTITEM_H
