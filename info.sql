/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 80021
Source Host           : localhost:3306
Source Database       : chatroom

Target Server Type    : MYSQL
Target Server Version : 80021
File Encoding         : 65001

Date: 2021-02-04 13:48:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for info
-- ----------------------------
DROP TABLE IF EXISTS `info`;
CREATE TABLE `info` (
  `account` int NOT NULL COMMENT '账号',
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL COMMENT '头像路径',
  PRIMARY KEY (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of info
-- ----------------------------
INSERT INTO `info` VALUES ('1001', 'godFather', '1001', ':/src/img/god.jpg');
INSERT INTO `info` VALUES ('1002', 'mike', '1002', ':/src/img/mike.jpg');
INSERT INTO `info` VALUES ('1003', 'kay', '1003', ':/src/img/kay.jpg');
INSERT INTO `info` VALUES ('1004', 'young', '1004', ':/src/img/young.jpg');
