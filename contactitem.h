﻿#ifndef CONTACTITEM_H
#define CONTACTITEM_H

#include <QWidget>

namespace Ui {
class contactItem;
}

class contactItem : public QWidget
{
    Q_OBJECT

public:
    explicit contactItem(QWidget *parent = nullptr);
    void setUsername(QString str);
    void setHeadPicture(QPixmap pixmap);
    QSize getHeadSize();
    ~contactItem();
private:
    Ui::contactItem *ui;
};

#endif // CONTACTITEM_H
