﻿#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include<QMouseEvent>
namespace Ui {
class login;
}

class login : public QWidget
{
    Q_OBJECT

public:
    explicit login(QWidget *parent = nullptr);
    void loadStyleSheet();
    bool connectMySql();
    ~login();
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
private slots:
    void on_loginBtn_clicked();

    void on_closeBtn_clicked();

private:
    Ui::login *ui;
    QPoint m_point;
};

#endif // LOGIN_H
