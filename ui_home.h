/********************************************************************************
** Form generated from reading UI file 'home.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOME_H
#define UI_HOME_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_home
{
public:
    QFrame *line;
    QWidget *bodyWidget;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QPushButton *chatBtn;
    QPushButton *addBtn;
    QWidget *topWidget;
    QPushButton *closeBtn;
    QLabel *headLabel;
    QLabel *nameLabel;
    QPushButton *setBtn;

    void setupUi(QWidget *home)
    {
        if (home->objectName().isEmpty())
            home->setObjectName(QString::fromUtf8("home"));
        home->resize(350, 500);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(home->sizePolicy().hasHeightForWidth());
        home->setSizePolicy(sizePolicy);
        home->setMinimumSize(QSize(350, 0));
        home->setMaximumSize(QSize(350, 16777215));
        line = new QFrame(home);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(0, 145, 500, 3));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(line->sizePolicy().hasHeightForWidth());
        line->setSizePolicy(sizePolicy1);
        line->setMinimumSize(QSize(500, 3));
        line->setMaximumSize(QSize(500, 3));
        line->setAutoFillBackground(false);
        line->setStyleSheet(QString::fromUtf8(""));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        bodyWidget = new QWidget(home);
        bodyWidget->setObjectName(QString::fromUtf8("bodyWidget"));
        bodyWidget->setGeometry(QRect(0, 145, 350, 355));
        sizePolicy.setHeightForWidth(bodyWidget->sizePolicy().hasHeightForWidth());
        bodyWidget->setSizePolicy(sizePolicy);
        bodyWidget->setMinimumSize(QSize(350, 0));
        bodyWidget->setMaximumSize(QSize(350, 16777215));
        widget = new QWidget(bodyWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(0, 0, 352, 42));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        chatBtn = new QPushButton(widget);
        chatBtn->setObjectName(QString::fromUtf8("chatBtn"));
        sizePolicy1.setHeightForWidth(chatBtn->sizePolicy().hasHeightForWidth());
        chatBtn->setSizePolicy(sizePolicy1);
        chatBtn->setMinimumSize(QSize(25, 25));
        chatBtn->setMaximumSize(QSize(25, 25));
        chatBtn->setStyleSheet(QString::fromUtf8(""));
        chatBtn->setFlat(false);

        horizontalLayout->addWidget(chatBtn);

        addBtn = new QPushButton(widget);
        addBtn->setObjectName(QString::fromUtf8("addBtn"));
        sizePolicy1.setHeightForWidth(addBtn->sizePolicy().hasHeightForWidth());
        addBtn->setSizePolicy(sizePolicy1);
        addBtn->setMinimumSize(QSize(25, 25));
        addBtn->setMaximumSize(QSize(25, 25));
        addBtn->setFlat(false);

        horizontalLayout->addWidget(addBtn);

        topWidget = new QWidget(home);
        topWidget->setObjectName(QString::fromUtf8("topWidget"));
        topWidget->setGeometry(QRect(0, 0, 350, 145));
        sizePolicy1.setHeightForWidth(topWidget->sizePolicy().hasHeightForWidth());
        topWidget->setSizePolicy(sizePolicy1);
        topWidget->setMinimumSize(QSize(350, 145));
        topWidget->setMaximumSize(QSize(350, 145));
        closeBtn = new QPushButton(topWidget);
        closeBtn->setObjectName(QString::fromUtf8("closeBtn"));
        closeBtn->setGeometry(QRect(323, 0, 27, 27));
        sizePolicy1.setHeightForWidth(closeBtn->sizePolicy().hasHeightForWidth());
        closeBtn->setSizePolicy(sizePolicy1);
        closeBtn->setMinimumSize(QSize(27, 27));
        closeBtn->setMaximumSize(QSize(27, 27));
        closeBtn->setFlat(false);
        headLabel = new QLabel(topWidget);
        headLabel->setObjectName(QString::fromUtf8("headLabel"));
        headLabel->setGeometry(QRect(20, 30, 80, 80));
        sizePolicy1.setHeightForWidth(headLabel->sizePolicy().hasHeightForWidth());
        headLabel->setSizePolicy(sizePolicy1);
        headLabel->setMinimumSize(QSize(80, 80));
        headLabel->setMaximumSize(QSize(80, 80));
        nameLabel = new QLabel(topWidget);
        nameLabel->setObjectName(QString::fromUtf8("nameLabel"));
        nameLabel->setGeometry(QRect(110, 80, 171, 30));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(nameLabel->sizePolicy().hasHeightForWidth());
        nameLabel->setSizePolicy(sizePolicy2);
        nameLabel->setMinimumSize(QSize(0, 30));
        nameLabel->setMaximumSize(QSize(16777215, 30));
        setBtn = new QPushButton(topWidget);
        setBtn->setObjectName(QString::fromUtf8("setBtn"));
        setBtn->setGeometry(QRect(326, 30, 24, 24));
        sizePolicy1.setHeightForWidth(setBtn->sizePolicy().hasHeightForWidth());
        setBtn->setSizePolicy(sizePolicy1);
        setBtn->setMinimumSize(QSize(24, 24));
        setBtn->setMaximumSize(QSize(24, 24));

        retranslateUi(home);

        QMetaObject::connectSlotsByName(home);
    } // setupUi

    void retranslateUi(QWidget *home)
    {
        home->setWindowTitle(QCoreApplication::translate("home", "Form", nullptr));
#if QT_CONFIG(tooltip)
        chatBtn->setToolTip(QCoreApplication::translate("home", "\350\201\212\345\244\251", nullptr));
#endif // QT_CONFIG(tooltip)
        chatBtn->setText(QString());
#if QT_CONFIG(tooltip)
        addBtn->setToolTip(QCoreApplication::translate("home", "\346\267\273\345\212\240\345\245\275\345\217\213", nullptr));
#endif // QT_CONFIG(tooltip)
        addBtn->setText(QString());
        closeBtn->setText(QString());
        headLabel->setText(QString());
        nameLabel->setText(QString());
#if QT_CONFIG(tooltip)
        setBtn->setToolTip(QCoreApplication::translate("home", "\350\256\276\347\275\256", nullptr));
#endif // QT_CONFIG(tooltip)
        setBtn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class home: public Ui_home {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOME_H
