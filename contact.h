﻿#ifndef CONTACT_H
#define CONTACT_H

#include <QWidget>
#include<QMouseEvent>
#include<QListWidgetItem>
namespace Ui {
class contact;
}

class contact : public QWidget
{
    Q_OBJECT

public:
    explicit contact(QWidget *parent = nullptr);
    ~contact();
private slots:
    void on_closeBtn_clicked();
    void onItemDoubleClicked(QListWidgetItem*item);
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
private:
    void initControl();
    void loadStyleSheet();
    QPixmap getRoundImg(const QPixmap & src,QPixmap & mask, QSize maskSize);//获取圆头像
    void getUserList(QStringList &userList);
    bool createJSFile(QStringList&userList);
private:
    Ui::contact *ui;
    QPoint m_point;
};

#endif // CONTACT_H
