﻿#ifndef MSGWEBVIEW_H
#define MSGWEBVIEW_H

#include <QWidget>
#include<QMouseEvent>
#include<QTcpSocket>
#include<QUdpSocket>
namespace Ui {
class msgWebView;
}

class msgWebView : public QWidget
{
    Q_OBJECT

public:
    explicit msgWebView(int account,QWidget *parent=nullptr);
    void loadStyleSheet();
    ~msgWebView();
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
private:
    void initTcpSocket();
    void initUdpSocket();
private slots:
    void on_closeBtn_clicked();
    void on_sendBtn_clicked();
     void dealData();//处理数据
signals:
    void deleteSrc();
private:
    Ui::msgWebView *ui;
    QPoint m_point;
    int m_account;//跟谁聊天
    QTcpSocket*m_tcpClient;
    QUdpSocket*m_udpReceiver;
};

#endif // MSGWEBVIEW_H
